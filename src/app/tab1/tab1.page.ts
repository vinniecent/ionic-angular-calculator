import { Component, OnInit } from '@angular/core';
import {Apollo, QueryRef, Subscription} from 'apollo-angular';
import gql from 'graphql-tag';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  categoryGroups: any[];
  loading = true;
  error: any;

  category:any
  usedOrNew:string = 'NEW'
  purchasePrice: number = 30000
  tenor: number = 36
  downPayment:number = 1500
  balloonPayment: number = 1
  calculateQuery: any

  calculation: string = ""
  boundaries: string = ""

  private querySubscription: Subscription;

  constructor(private apollo: Apollo) {}

  calculate() {

    const cat = this.categoryGroups.find(value => value.id == this.category)?.categories[0]?.id

    if (!cat) {
      alert('cat cannot be null')
      return
    }

    this.calculateQuery.refetch({
        input : {
          balloonPayment: this.balloonPayment,
          categoryId: cat,
          downPayment: this.downPayment,
          objectUsed: this.usedOrNew === 'USED',
          objectYear: 2020,
          operational: false,
          purchasePrice: this.purchasePrice,
          tenor: this.tenor,
          withSale: false,
        }
    })
  }

  ngOnInit() {
    this.apollo
    .watchQuery({
      query: gql`
        {
          categoryGroups {
            id
            name
            categories {
              id
              name
              defaultForCategoryGroup
              supportedTypesOfFinance
            }
          }
        }
      `,
    })
    .valueChanges.subscribe((result:any)  => {
      this.categoryGroups = result?.data?.categoryGroups
      this.loading = result.loading;
      this.error = result.error;
    });


    this.calculateQuery = this.apollo
    .watchQuery({
      query: gql`
        query Foo($input: CalculateLeaseInput!) {
          calculateLease(input: $input) {
            calculation {
              monthlyPayment
              downPayment
              balloonPayment
              tenor
              handlingFee
              totalCosts
            }
            boundaries {
              minBalloonPayment
              maxBalloonPayment
              minDownPayment
              maxDownPayment
              minTenor
              maxTenor
            }
          }
        }
      `,
      variables: {
        input : {
          balloonPayment: this.balloonPayment,
          categoryId: 9999,
          downPayment: this.downPayment,
          objectUsed: this.usedOrNew === 'USED',
          objectYear: 2020,
          operational: false,
          purchasePrice: this.purchasePrice,
          tenor: this.tenor,
          withSale: false,
        }
      },
    })


    this.calculateQuery.valueChanges.subscribe((result:any)  => {

      this.calculation = "Calculation: " + JSON.stringify(result?.data?.calculateLease?.calculation)
      this.boundaries = "Boundaries: " + JSON.stringify(result?.data?.calculateLease?.boundaries)
    })

  }
}
